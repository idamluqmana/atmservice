package view;

import static constants.ViewConstant.*;

public class ATMView{

    public void greeting(String username) {
        System.out.println(HELLO_TEXT+" "+username);
    }

    public void showBalance(int balance) {
        System.out.println(YOUR_BALANCE+""+balance);
    }

    public void alertLoginFirst() {
        System.out.println(MUST_LOGIN);
    }
    
    public void alertBalanceNotEnough() {
        System.out.println(YOUR_BALANCE_NOT_ENOUGH);
    }

    public void transferSuccess(String username,int number) {
        System.out.println(TRANSFERED+""+number+ " to "+username);
    }

    public void alreadyLogin(){
        System.out.println(ALREADY_LOGIN);
    }

    public void numberAlertNull(){
        System.out.println(DEPOSITNULL);
    }

    public void transferAccountNotFound(){
        System.out.println(TRANSFERACCOUNTNOTFOUND);
    }

    public void goodbye(String username){
        System.out.println(GOODBYE_TEXT+" "+username);
    }

    public void initial(){
        System.out.println("```bash");
        System.out.print("$ ");
    }

    public void commandNotFound(){
        System.out.println(COMMAND_NOT_FOUND);
    }

    public void accountEmptyValidation(){
        System.out.println(INPUTACCOUNTEMPTYVALIDSTION);
    }
}
