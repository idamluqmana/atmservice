package model.response;

public class ResponseTransfer {
       private String userTarget;
       private  int amount;

    public ResponseTransfer(String userTarget, int amount) {
        this.userTarget = userTarget;
        this.amount = amount;
    }

    public String getUserTarget() {
        return userTarget;
    }

    public int getAmount() {
        return amount;
    }
}
