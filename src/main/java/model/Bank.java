package model;

import model.response.ResponseTransfer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Bank {
    private List<Account> accounts;

    public Bank(List<Account> accounts) {
        this.accounts = accounts;
    }

    public void insertNewAccount(Account account) {
        this.accounts.add(account);
    }

    public Account findAccount(String username) {
        Account account = new Account();
        List<Account> filterAccount = new ArrayList<>();
        if (!accounts.isEmpty()) {
            filterAccount = this.accounts.stream()
                    .filter(a -> Objects.equals(a.getName(), username))
                    .collect(Collectors.toList());
            if(!filterAccount.isEmpty()){
                account = filterAccount.get(0);
            }
        }
        return account;

    }

    public ResponseTransfer transfer(String usernameFrom, String usernameTo, int amount) {
        Account accountFrom = this.findAccount(usernameFrom);
        Account accountTo = this.findAccount(usernameTo);
        accountFrom.decreaseBalance(amount);
        accountTo.increaseBalance(amount);
        return new ResponseTransfer(usernameTo, amount);
    }

    public void withDraw(String username, int amount) {
        Account account = this.findAccount(username);
        account.decreaseBalance(amount);
    }

    public void deposit(String username, int amount) {
        Account account = this.findAccount(username);
        account.increaseBalance(amount);
    }
}
