package model;

public class CommandFactory extends Command{
    public CommandFactory() {
        super();
    }
    public Command inputCommand(String input){
        String[] splited = input.split(" ");
        String action = splited[0];
        Command command = new Command(action,splited);
        return command;
    }
}
