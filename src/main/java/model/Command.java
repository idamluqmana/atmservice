package model;

public class Command {
    private String action;
    private String[] args;

    public Command() {}

    public Command(String action, String[] args) {
        this.action = action;
        this.args = args;
    }

    public String getAction() {
        return action;
    }

    public String[] getArgs() {
        return args;
    }
}
