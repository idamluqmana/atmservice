package model;

public class Account {
    private String name;
    private int balance;

    public Account() {
    }

    public Account(String name, int balance) {
        this.name = name;
        this.balance = balance;
    }

    public void increaseBalance(int amount) {
        this.balance = this.balance + amount;
    }

    public void decreaseBalance(int amount) {
        this.balance = this.balance - amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }


}
