package constants;

public class CommandConstant {
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String DEPOSIT = "deposit";
    public static final String TRANSFER = "transfer";
    public static final String WITHDRAW = "withdraw";
    public static final String EXIT = "exit";
}
