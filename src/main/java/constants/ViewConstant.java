package constants;

public class ViewConstant {
    public static String HELLO_TEXT = "Hello, ";
    public static String YOUR_BALANCE = "Your balance is ";
    public static String MUST_LOGIN = "Please login first !";
    public static String CANNOT_TRANSFER_SAME_ACCOUNT = "You cant tranfer on your account";
    public static String YOUR_BALANCE_NOT_ENOUGH = "Your balance is not enough";
    public static String TRANSFERED = "Tranfered $";
    public static String TO = "to";

    public static String TRANSFERACCOUNTNOTFOUND = "The account that you want to tranfer is not found";

    public static String ALREADY_LOGIN = "You are already login, please logout first.";

    public static String INPUTACCOUNTEMPTYVALIDSTION= "Account name cannot be empty";

    public static String GOODBYE_TEXT = "Goodbye, ";

    public static String DEPOSITNULL = " Number input cannot be empty";

    public static String COMMAND_NOT_FOUND = "Sorry, command not found!";

}
