
import constants.CommandConstant;
import controller.ATMController;
import model.Command;
import model.CommandFactory;
import runner.ATMRunner;
import view.ATMView;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ATMRunner runner = new ATMRunner();
        runner.runCommand();
    }
}
