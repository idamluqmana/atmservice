package runner;

import constants.CommandConstant;
import controller.ATMController;
import model.Command;
import model.CommandFactory;
import view.ATMView;

import java.util.Scanner;

public class ATMRunner {
    ATMView atmView = new ATMView();
    ATMController controller = new ATMController();

    static Scanner key = new Scanner(System.in);
    public void runCommand() {
        System.out.println("ATM Simulator by Idam");
        boolean isQuit = false;
        while(!isQuit) {
            atmView.initial();
            String action = key.nextLine();
            Command command = new CommandFactory().inputCommand(action);
            switch (command.getAction()) {
                case CommandConstant.LOGIN:
                    if (command.getArgs().length < 2) {
                        atmView.accountEmptyValidation();
                    } else {
                        controller.login(command.getArgs()[1]);
                    }
                    break;
                case CommandConstant.DEPOSIT:
                    if (command.getArgs().length < 2) {
                        atmView.numberAlertNull();
                    } else {
                        controller.deposit(Integer.parseInt(command.getArgs()[1]));
                    }
                    break;
                case CommandConstant.WITHDRAW:
                    if (command.getArgs().length < 2) {
                        atmView.numberAlertNull();
                    } else {
                        controller.withdraw(Integer.parseInt(command.getArgs()[1]));
                    }
                    break;
                case CommandConstant.TRANSFER:
                    if (command.getArgs().length < 2) {
                        atmView.transferAccountNotFound();
                    } else if (command.getArgs().length < 3) {
                        atmView.numberAlertNull();
                    } else {
                        controller.transfer(command.getArgs()[1], Integer.parseInt(command.getArgs()[2]));
                    }
                    break;
                case CommandConstant.LOGOUT:
                    controller.logout();
                    break;
                case CommandConstant.EXIT:
                    isQuit = true;
                    break;
                default:
                    atmView.commandNotFound();
                    break;

            }
        }
    }
}
