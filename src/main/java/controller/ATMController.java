package controller;

import model.Account;
import model.Bank;
import model.response.ResponseTransfer;
import view.ATMView;

import java.util.ArrayList;
import java.util.List;

public class ATMController {

    private Bank bank;
    private String activeUser;
    List<Account> list = new ArrayList();

    public ATMController() {
        this.bank = new Bank(list);
        this.activeUser = null;
    }

    public void login(String username) {
        ATMView atmView = new ATMView();
        if (this.activeUser == null) {
            Account account = this.bank.findAccount(username);
            atmView.greeting(username);
            atmView.showBalance(account.getBalance());
            if(account.getName() == null){
                Account newAccount = new Account(username, 0);
                this.bank.insertNewAccount(newAccount);
            }
            this.activeUser = username;
        } else {
            atmView.alreadyLogin();
        }
    }

    public void deposit(int deposit) {
        ATMView atmView = new ATMView();
        if (this.activeUser != null) {
            Account account = bank.findAccount(activeUser);
            bank.deposit(activeUser,deposit);
            atmView.showBalance(account.getBalance());
        } else {
            atmView.alertLoginFirst();
        }
    }

    public void withdraw(int number) {
        ATMView atmView = new ATMView();
        if (this.activeUser != null) {
            Account account = bank.findAccount(activeUser);
            if(account.getBalance() > number){
                bank.withDraw(activeUser,number);
                atmView.showBalance(account.getBalance());
            }else{
                atmView.alertBalanceNotEnough();
            }
        } else {
            atmView.alertLoginFirst();
        }
    }

    public void transfer(String destination, int number) {
        ATMView atmView = new ATMView();
        if (this.activeUser != null) {
            Account fromAccount = bank.findAccount(activeUser);
            if(fromAccount.getBalance() > number){
                Account toAccount = bank.findAccount(destination);
                System.out.println(toAccount.getName());
                if(toAccount.getName() != null){
                    ResponseTransfer responseTranfer = bank.transfer(activeUser,toAccount.getName(),number);
                    atmView.transferSuccess(responseTranfer.getUserTarget(),responseTranfer.getAmount());
                    atmView.showBalance(fromAccount.getBalance());
                }else {
                    atmView.transferAccountNotFound();
                }
            }else{
                atmView.alertBalanceNotEnough();
            }
        } else {
            atmView.alertLoginFirst();
        }
    }
    public void logout() {
        ATMView atmView = new ATMView();
        atmView.goodbye(activeUser);
        this.activeUser = null;
    }
}