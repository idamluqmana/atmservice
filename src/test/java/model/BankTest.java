package model;

import model.response.ResponseTransfer;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BankTest {

    @Test
    public void insertNewAccountTest() {
        Account newAccount= new Account("idam",0);
        /* Given */ List<Account> accounts = new ArrayList<>();
        Bank bank = new Bank(accounts);
        /* When */  bank.insertNewAccount(newAccount);
        assertEquals(1, accounts.size());
    }

    @Test
    public void findAccountTest() {
        Account newAccount= new Account("idam",0);
        /* Given */ List<Account> accounts = new ArrayList<>();
        accounts.add(newAccount);
        Bank bank = new Bank(accounts);
        /* When */  Account account = bank.findAccount("idam");
        assertEquals("idam", account.getName());
    }


    @Test
    public void transferTest() {
        /* Given */
        Account fromAccount= new Account("idam",100);
        Account toAccount= new Account("apang",0);
        List<Account> accounts = new ArrayList<>();
        accounts.add(fromAccount);
        accounts.add(toAccount);
        Bank bank = new Bank(accounts);
        /* When */  Account account1 = bank.findAccount("idam");
        Account account2 = bank.findAccount("apang");
        ResponseTransfer responseTranfer = bank.transfer("idam","apang",10);
        assertEquals(10, account2.getBalance());
        assertEquals(90, account1.getBalance());
        assertEquals(10, responseTranfer.getAmount());
    }


    @Test
    public void depositTest() {
        /* Given */
        Account fromAccount= new Account("idam",100);
        List<Account> accounts = new ArrayList<>();
        accounts.add(fromAccount);
        Bank bank = new Bank(accounts);
        /* When */  Account account1 = bank.findAccount("idam");
        bank.deposit("idam",10);
        assertEquals(110, account1.getBalance());
    }

    @Test
    public void withdrawTest() {
        /* Given */
        Account fromAccount= new Account("idam",100);
        List<Account> accounts = new ArrayList<>();
        accounts.add(fromAccount);
        Bank bank = new Bank(accounts);
        /* When */  Account account1 = bank.findAccount("idam");
        bank.withDraw("idam",10);
        assertEquals(90, account1.getBalance());
    }


}
