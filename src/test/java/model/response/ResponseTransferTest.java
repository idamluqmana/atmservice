package model.response;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ResponseTransferTest {

    @Test
    public void getUserTargetTest() {
        /* Given */
        ResponseTransfer responseTransfer = new ResponseTransfer("idam",0);
        /* When */  String result = responseTransfer.getUserTarget();
        assertEquals("idam", result);
    }

    @Test
    public void getAmountTest() {
        /* Given */
        ResponseTransfer responseTransfer = new ResponseTransfer("idam",0);
        /* When */  int result = responseTransfer.getAmount();
        assertEquals(0, result);
    }
}
