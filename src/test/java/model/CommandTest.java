package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommandTest {

    @Test
    public void getCommandTest() {
        /* Given */
        String[] args = new String[]{"idam"};
        Command command = new Command("login",args);
        /* When */  String result = command.getAction();
        assertEquals("login", result);
    }

    @Test
    public void getAmountTest() {
        /* Given */
        String[] args = new String[]{"idam"};
        Command command = new Command("login",args);
        /* When */  String result = command.getArgs()[0];
        assertEquals("idam", result);
    }

}
