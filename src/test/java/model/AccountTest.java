package model;

import model.Account;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class AccountTest {

    @Test
    public void incereaseBalanceTest() {
        /* Given */ Account account = new Account();
        /* When */  account.increaseBalance(10);
        assertEquals(10, account.getBalance());
    }

    @Test
    public void decereaseBalanceTest() {
        /* Given */ Account account = new Account();
        /* When */  account.decreaseBalance(10);
        assertEquals(-10, account.getBalance());
    }
    @Test
    public void getNameTest() {
        /* Given */ Account account = new Account();
        account.setName("idam");
        /* When */  String result = account.getName();
        assertEquals("idam", result);
    }

    @Test
    public void getBalanceTest() {
        /* Given */ Account account = new Account();
        account.setBalance(0);
        /* When */  int result = account.getBalance();
        assertEquals(0, result);
    }


    @Test
    public void setNameTest() {
        /* Given */ Account account = new Account();
        /* When */  account.setName("idam");
        assertEquals("idam", account.getName());
    }

    @Test
    public void setBalanceTest() {
        /* Given */ Account account = new Account();
        /* When */  account.setBalance(0);
        assertEquals(0, account.getBalance());
    }
}