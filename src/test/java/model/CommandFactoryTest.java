package model;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommandFactoryTest {

    @Test
    public void inputCommandTest(){
        String input = "login idam";
        String[] splited = input.split(" ");
        Command command = new CommandFactory().inputCommand(splited[0]);
        assertEquals("login", command.getAction());
    }
}
