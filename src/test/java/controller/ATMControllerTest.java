package controller;

import model.Account;
import model.Bank;
import org.junit.Test;
import view.ATMView;
import static org.mockito.Mockito.*;

public class ATMControllerTest {
    Bank bank = mock(Bank.class);
    ATMView atmView = mock(ATMView.class);

    @Test
    public void loginExistingAccount() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).greeting("idam");
        doNothing().when(atmView).showBalance(0);
        controller.login("idam");
        verifyNoMoreInteractions(bank);
//        verify(bank, times(1)).insertNewAccount(account1);
    }

    @Test
    public void loginNewAccount() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).greeting("idam");
        doNothing().when(atmView).showBalance(0);
        controller.login("idam");
        verifyNoMoreInteractions(bank);
//        verify(bank, times(1)).insertNewAccount(account1);
    }

    @Test
    public void alreadyLoginAccount() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).greeting("idam");
        doNothing().when(atmView).showBalance(0);
        controller.login("idam");
        controller.login("idam");
        verifyNoMoreInteractions(bank);
//        verify(bank, times(1)).insertNewAccount(account1);
    }
//
    @Test
    public void depositTestSuccess() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).showBalance(0);
        controller.login("idam");
        controller.deposit(20);
        verifyNoMoreInteractions(bank);
    }

    @Test
    public void depositTestErrorLoginFirst() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).showBalance(0);
        controller.deposit(20);
        verifyNoMoreInteractions(bank);
    }


    @Test
    public void withdrawTestSuccess() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).showBalance(0);
        controller.login("idam");
        controller.deposit(30);
        controller.withdraw(10);
        verifyNoMoreInteractions(bank);
    }

    @Test
    public void withdrawTestNotEnoughBalance() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).showBalance(0);
        controller.login("idam");
        controller.deposit(30);
        controller.withdraw(40);
        verifyNoMoreInteractions(bank);
    }


    @Test
    public void transferTestNotEnoughBalance() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).showBalance(0);
        controller.login("idam");
        controller.deposit(30);
        controller.transfer("apang",40);
        verifyNoMoreInteractions(bank);
    }

    @Test
    public void withdrawTestLoginFirst() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).showBalance(0);
        controller.withdraw(40);
        verifyNoMoreInteractions(bank);
    }

    @Test
    public void transferTestLoginFirst() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).showBalance(0);
        controller.transfer("apang",40);
        verifyNoMoreInteractions(bank);
    }

    @Test
    public void transferTestTransferSuccess() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).showBalance(0);
        controller.login("idam");
        controller.logout();
        controller.login("apang");
        controller.deposit(20);
        controller.transfer("idam",10);
        verifyNoMoreInteractions(bank);
    }

    @Test
    public void transferTestAccountNotFound() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(account1).when(bank).findAccount("idam");
        doNothing().when(atmView).showBalance(0);
        controller.login("apang");
        controller.deposit(20);
        controller.transfer("idam",10);
        verifyNoMoreInteractions(bank);
    }


    @Test
    public void tranferTestErrorTranferAccountNotFound() {
        /* Given */
        Account account1 = new Account();
        ATMController controller = new ATMController();
        /* When */
        doReturn(null).when(bank).findAccount("idam");
        doNothing().when(atmView).showBalance(0);
        controller.login("apang");
        controller.deposit(20);
        controller.transfer(null,10);
        verifyNoMoreInteractions(bank);
    }
}
