package view;


import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class ATMViewTest {

    @Test
    public void greeting() {
        ATMView atmView = new ATMView();
        atmView.greeting("idam");
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        String allWrittenLines = new String(bo.toByteArray());
        System.out.println(allWrittenLines);
        assertFalse(allWrittenLines.equals("Hello,  idam"));
    }


    @Test
    public void showBalance() {
        ATMView atmView = new ATMView();
        atmView.showBalance(0);
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        String allWrittenLines = new String(bo.toByteArray());
        assertFalse(allWrittenLines.contains("Your balance is 0"));
    }


    @Test
    public void alertLoginFirst() {
        ATMView atmView = new ATMView();
        atmView.alertLoginFirst();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        String allWrittenLines = new String(bo.toByteArray());
        assertFalse(allWrittenLines.contains("Please login first !"));
    }

    @Test
    public void alertBalanceNotEnough() {
        ATMView atmView = new ATMView();
        atmView.alertBalanceNotEnough();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        String allWrittenLines = new String(bo.toByteArray());
        assertFalse(allWrittenLines.contains("Your balance is not enough"));
    }


    @Test
    public void transferSuccess() {
        ATMView atmView = new ATMView();
        atmView.transferSuccess("idam", 10);
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        String allWrittenLines = new String(bo.toByteArray());
        assertFalse(allWrittenLines.contains("Tranfered $10 to idam"));
    }


    @Test
    public void alreadyLogin() {
        ATMView atmView = new ATMView();
        atmView.alreadyLogin();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        String allWrittenLines = new String(bo.toByteArray());
        assertFalse(allWrittenLines.contains("You are already login, please logout first."));
    }

    @Test
    public void numberAlertNull() {
        ATMView atmView = new ATMView();
        atmView.numberAlertNull();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        String allWrittenLines = new String(bo.toByteArray());
        assertFalse(allWrittenLines.contains(" Number input cannot be empty"));
    }

    @Test
    public void transferAccountNotFound() {
        ATMView atmView = new ATMView();
        atmView.transferAccountNotFound();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        String allWrittenLines = new String(bo.toByteArray());
        assertFalse(allWrittenLines.contains("The account that you want to tranfer is not found"));
    }


    @Test
    public void goodbye() {
        ATMView atmView = new ATMView();
        atmView.goodbye("idam");
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        String allWrittenLines = new String(bo.toByteArray());
        assertFalse(allWrittenLines.contains("Goodbye,  idam"));
    }

    @Test
    public void initial() {
        ATMView atmView = new ATMView();
        atmView.initial();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        String allWrittenLines = new String(bo.toByteArray());
        assertFalse(allWrittenLines.contains("```bash $"));
    }

    @Test
    public void accountEmptyValidation() {
        ATMView atmView = new ATMView();
        atmView.accountEmptyValidation();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        String allWrittenLines = new String(bo.toByteArray());
        assertFalse(allWrittenLines.contains("Account name cannot be empty"));
    }


}
