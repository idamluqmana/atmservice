
# ATM service

Click for more details on coverage:

[![coverage](https://gitlab.com/idamluqmana/atmservice/badges/main/coverage.svg?job=testversion)](https://idamluqmana.gitlab.io/atmservice/)


## About The Project

  

A sample command-line application build with java language and have an entrypoint in `main/java/`, and unit test in every directory with name `test/java/`. This project is using OOP approach with MVC pattern. This pattern is used to separate application's concerns.

among others are: Model, Controller, View. and supported by other helper like Reader and Runner.

  

## _Prerequisites_

Make sure you have [Git](https://git-scm.com/) installed in your local. You can clone this project with this [link](https://gitlab.com/idamluqmana/atmservice) or you can visit this project at [gitlab](https://gitlab.com/idamluqmana/atmservice). This project is written using [Java] version 11.0.9. Please install java with same version before you run this console-app, you can see the direction [here](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html).

  

To check if is already installed. You can open `terminal` and type `java --version`, make sure that java version in your local is >= 11.0.0.

  

## Problem Statement

  

You are asked to develop a Command Line Interface (CLI) to simulate an interaction of an ATM Machine.

  

### Commands

*  `login [name]` - Logs in as this customer and creates the customer if not exist

  

*  `deposit [amount]` - Deposits this amount to the logged in customer

  

*  `withdraw [amount]` - Withdraws this amount from the logged in customer

  

*  `transfer [target] [amount]` - Transfers this amount from the logged in customer to the target customer

  

*  `logout` - Logs out of the current customer

  

## How to run?

  

After cloning this project, you can follow the instruction below:

  
* open your Intelij and open project base folder

* get all dependencies that all need in this application by running this command:

```bash
mvn dependency:resolve
``` 

* running test by running this command:

```bash
mvn clean test
```

* running maven project by running this command:

```bash
mvn clean compile exec:java
```

* and you will see this cli interface below:

```bash

ATM Simulator by Idam
```bash
$ 

```

  

## Example Result

  

Users can interact with the ATM system via a following simple set of commands which produce a specific output:

  

-  `login user` will create customer with username `alice`

```bash

$ login idam

Hello, idam

Your balance is $0

```
-  `deposit 100` will increase balance to logged in customer in this case `user`.

```bash

$ deposit 100

Your balance is $100

```

-  `transfer user 120` will transfer 120 amount to username `user`

  

```bash

$ transfer user 120

Transferred $100 to user

Your balance is $0

```

-  `withdraw 100` will decrease 100 amount from logged in customer balance

```bash

$ withdraw 100

Your balance is $100

```

-  `logout` will logging out the active user

```bash

$ logout

Goodbye, idam

```

  

-  **exit**: `exit` will quit the application and return to the console.

  

>  **NOTE**: Any commands which are not mentioned above will show an error: `Sorry, command not found!`.

  

## Folder Structure

```bash

.

│

├── /src

│ ├── /main

│ │ ├── /constant

│ │ │ ├── CommandConstant.java

│ │ │ └── ViewConstant.java

│ │ └── /controller

│ │ │ └── ATMController.java

│ │ ├── /model

│ │ │ └── response

│ │ │ │ └── ResponseTransfer.java

│ │ │ └── Account.java

│ │ │ ├── Bank.java

│ │ │ ├── Command.java

│ │ │ ├── CommandFactory.java

│ │ └── view

│ │ │ ├── ATMView.java

│ │

│ ├── /test

│ │ └── /controller

│ │ │ └── ATMControllerTest.java

│ │ ├── /model

│ │ │ └── response

│ │ │ │ └── ResponseTransferTest.java

│ │ │ └── AccountTest.java

│ │ │ ├── BankTest.java

│ │ │ ├── CommandTest.java

│ │ │ ├── CommandFactoryTest.java

│ │ └── view

│ │ │ ├── ATMViewTest.java

│ │ ├── Main.java


```

## Owner

[@idamluqmana_](https://instagram.com/idamluqmana_)

Idam Luqmana
